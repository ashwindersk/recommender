import logging
from datetime import datetime

from flask import Flask, request
from flask_restx import Resource, Api, fields

from recommendation_service import recommendation as rc

LOG = logging.getLogger(__name__)

app = Flask(__name__)
api = Api(app)

ns = api.namespace('v1/recommendation', decription = "Get a recommendation for a ticker")

recommendation = api.model('Recommendation', 
                            {
                                'tickers': fields.List(required=True, cls_or_instance=fields.String),
                                'date'   : fields.String(required = True)
                            })


@ns.route("/")
class recommendation_controller(Resource):

    @ns.expect(recommendation)
    def post(self):
        LOG.info("Post request received. Date: " + api.payload['date'] +". Tickers: " +", ".join(api.payload['tickers']))

        self.recommendation = rc(num_mavg = 9)
        date = datetime.strptime(api.payload['date'], '%Y-%m-%d').date()

        self.recommendation.set_date(date)
        self.recommendation.set_tickers(api.payload['tickers'])

        recommendations = self.recommendation.get_recommendations()

        LOG.info(recommendations)
        return recommendations

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    app.run(debug=True)
