import pandas as pd 

from pandas_datareader import data
from datetime import datetime
from datetime import timedelta
from enum import Enum
import sys
import logging



class ACTION(Enum):
    BUY = 1
    SELL = -1
    HOLD = 0

class recommendation:

    def __init__(self, num_mavg):
        self.num_mavg        = num_mavg
    
    def set_tickers(self,tickers):
        self.tickers = tickers
    
    def set_date(self, date):
        self.end = date
        self.start = date - timedelta(days = 30)
    
    def get_recommendations(self):
        return [self.recommend(ticker, self.get_ticker_data(ticker)) for ticker in self.tickers]

    def get_ticker_data(self, ticker):
        try:

            ticker_data = data.DataReader(ticker, start=self.start, end=self.end, data_source='yahoo')
        
        except KeyError:
            print("Invalid ticker")
            return
        except:
            print(sys.exc_info()[0])
            return

        ticker_data[f'{self.num_mavg} Mavg'] = ticker_data['Close'].rolling(window = self.num_mavg).mean()
        ticker_data['Std']                   = ticker_data['Close'].rolling(window = self.num_mavg).std()
        ticker_data['BB Upper']              = ticker_data[f'{self.num_mavg} Mavg'] + 2*ticker_data['Std']
        ticker_data['BB Lower']              = ticker_data[f'{self.num_mavg} Mavg'] - 2*ticker_data['Std']

        return ticker_data


    def recommend(self, ticker, ticker_data):
        current_mavg = ticker_data[f'{self.num_mavg} Mavg'][-1]
        bb_upper     = ticker_data['BB Upper'][-1]
        bb_lower     = ticker_data['BB Lower'][-1]
        price        = ticker_data['Close'][-1]

        action = ACTION.HOLD
        if  current_mavg > bb_upper:
            action = ACTION.SELL
        
        elif current_mavg < bb_lower:
            action = ACTION.BUY
        
        else:
            pass

        return {ticker:{"action": action.value, "price":price }}